/*
 * This code was written by Paul Roberts <pmr@stelo.org.uk> and is in the public
 * domain, do whatever you want with it
 */

(function () {
    "use strict";
    var done = {
        html_util: false
    };
    /* if called, this function will add some useful tools to the prototype for
     * some HTML elements */
    function html_util() {
        // only do it once
        if (done.html_util) {
            return;
        }
        HTMLSelectElement.prototype.getValue = function () {
            return this.item(this.selectedIndex).value;
        };
        done.html_util = true;
    };

    /* This function will find named tags in a string, and replace them with
     * the string values of the property in the second argument with the same
     * name.
     *
     * A tag looks like this: %{name} Hide a % (and therefore a tag) with %{%},
     * which will get replaced with a single %
     *
     * Unknown tags, or non-string values, will be silently ignore, that is, the
     * tag will be replaced with the empty string
     */

    function tag_replace(str, tags) {
        var tagre = RegExp("%{([^}]+)}", "g");

        return str.replace(tagre, function (whole_match, tag_name) {
            if (tag_name == "%") {
                return "%";
            } else if (typeof tags[tag_name] == "string") {
                return tags[tag_name];
            } else {
                return "";
            }
        });
    }

    var steltools = {
        tag_replace: tag_replace,
        html_util: html_util
    }

    if (typeof define == "function") {
        define("steltools", steltools);
    } else if (typeof module == "object") {
        module.exports = steltools;
    } else if (typeof window != "undefined") {
        window.steltools = steltools;
    }
})();
